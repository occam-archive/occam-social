# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log import Log

from occam.manager import uses
from occam.commands.manager import command, option, argument

from occam.social.manager import SocialManager
from occam.objects.manager     import ObjectManager

@command('social', 'comment',
  category      = 'Social',
  documentation = "Posts a comment to the given object.")
@argument("object", type="object")
@argument("comment")
@option("-r", "--reply-to",  dest    = "reply_to",
                             action  = "store",
                             help    = "Replies to a given comment ID")
@option("-a", "--anonymous", dest    = "anon",
                             action  = "store_true",
                             help    = "Comment anonymously")
@uses(SocialManager)
@uses(ObjectManager)
class CommentCommand:
  def do(self):
    Log.header("Adding comment")

    # Get the object
    obj = self.objects.resolve(self.options.object, person = self.person)

    # Fail out if the object doesn't exist
    if obj is None:
      Log.error("Cannot find the given object")
      return -1

    # Insert the comment
    record = self.social.addComment(obj, self.person, self.options.comment,
                                    anonymous   = self.options.anon,
                                    in_reply_to = self.options.reply_to)

    ret = self.social.serializeComment(record)

    import json
    Log.output(json.dumps(ret))

    Log.done("Successfully commented")
    return 0
