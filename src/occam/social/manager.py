# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#TODO: This module.
import os

from occam.manager import uses, manager

from occam.objects.manager import ObjectManager

@manager("social")
@uses(ObjectManager)
class SocialManager:
  """ Manages evaluation plans within the node or federation.
  """

  def aliasFor(self, obj, person):
    """ Creates the alias for the given object and person for anonymous comments.

    Returns:
        str: An alias that anonymously identifies a person for an object.
    """

    # TODO: encrypt (xor) with a random secret generated once per system or once per
    #       object with the same length as the biggest hash (sha224 is 61 characters)

    # This is essentially what an encrypted HMAC is, so maybe that's a better route

    import hashlib
    token = "%s-%s" % (obj.uuid, person.uuid)

    return hashlib.sha224(token.encode('utf-8')).hexdigest()

  def addComment(self, obj, person, comment, anonymous=False, in_reply_to=None):
    """ Comment on an object.

    Args:
        obj (Object): The Occam object to attach the comment.
        person (Person): The Occam person that authored the comment.
        comment (str): The markdown content of the comment.
        in_reply_to (str): The comment uuid this comment is responding to.

    Returns:
        CommentRecord: The record inserted into the database representing the new comment.
    """

    person_name = None
    person_uuid = None
    alias = None
    if isinstance(person, str):
      person_name = person
    elif person is not None:
      person_uuid = person.uuid
      person_name = self.objects.infoFor(person).get('name')

      if anonymous:
        alias = self.aliasFor(obj, person)
        person_uuid = None
        person_name = None

    return self.datastore.insertComment(obj, person_uuid, person_name, alias, comment, in_reply_to)

  def listComments(self, obj, comment_id = None, in_reply_to = None, full_chain = False):
    """ Retrieve a list of comments for the given object.

    Args:
        obj (Object): The Occam object to poll for comments.

    Returns:
        list: A set of CommentRecord items for each comment in the object.
    """

    return self.datastore.retrieveComments(obj, commentId = comment_id, inReplyTo = in_reply_to, fullChain = full_chain, withReplies = False)

  def listCommentsWithReplies(self, obj, comment_id = None, in_reply_to = None, full_chain = False):
    """ Retrieve a list of comments for the given object along with direct replies.

    To gather more replies, you will have to query again.

    Args:
        obj (Object): The Occam object to poll for comments.

    Returns:
        list: A set of CommentRecord items for each comment in the object.
    """

    return self.datastore.retrieveComments(obj, commentId = comment_id, inReplyTo = in_reply_to, fullChain = full_chain, withReplies = True)

  def serializeComment(self, comment):
    """ Serializes the given CommentRecord into the eventual JSON output.
    """

    # The initial serialized form (num_replies is a generated column)
    item = {
      "id":         comment.uid,
      "createTime": comment.create_time.isoformat(),
      "content":    comment.content,
    }

    if "num_replies" in comment._data:
      item["numReplies"] = comment._data["num_replies"]

    # Pass along what this comment is a reply to
    if comment.in_reply_to:
      item["inReplyTo"] = comment.in_reply_to

    # Pass along the alias
    if comment.anonymous_alias:
      item["alias"] = comment.anonymous_alias

    # We can speed up this check later by gathering the normalized
    #   record of this Person the same time we query the comments.
    # (though we don't normalized the avatar images, sadly, yet)
    person = None
    if comment.commenter_uid:
      item['person'] = {
        "id":   comment.commenter_uid,
        "name": comment.commenter_name
      }

      person = self.objects.retrieve(uuid = comment.commenter_uid)
      if person:
        personInfo = self.objects.infoFor(person)

        item['person'].update({
          "revision": person.revision,
          "type":     personInfo.get('type'),
          "name":     personInfo.get('name')
        })

        if personInfo:
          item['person']['images'] = personInfo.get('images', [])

    return item
